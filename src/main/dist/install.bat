@echo off
echo.
echo El Programador Errante - Instalacion
echo ------------------------------------
echo.
echo Generando script de ejecucion...
copy /Y install.1.dat wd.vbs
echo WshShell.Run "javaw -jar ""%~dp0wd.jar""" >> wd.vbs
echo.
echo Creando acceso directo en el escritorio...
powershell "$shell=(New-Object -COM WScript.Shell);$shortcut=$shell.CreateShortcut('%userprofile%\Desktop\Programador Errante.lnk');$shortcut.TargetPath='%~dp0wd.vbs';$shortcut.IconLocation='%~dp0wd.ico';$shortcut.WorkingDirectory='%~dp0';$shortcut.WindowStyle=1;$shortcut.Save();"
echo.
echo Copiando acceso directo en menu de inicio...
copy "%userprofile%\Desktop\Programador Errante.lnk" "C:\Users\%username%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Programador Errante.lnk"
echo.
echo Generando script de desinstalacion....
copy /Y install.2.dat uninstall.bat
echo echo Eliminando datos de la instalacion... >> uninstall.bat
echo del /Q "%~dp0wd.vbs" >> uninstall.bat
echo echo.>>uninstall.bat
echo echo Eliminando acceso directo del menu de inicio...>>uninstall.bat
echo del /Q "C:\Users\%username%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Programador Errante.lnk">>uninstall.bat
echo echo.>>uninstall.bat
echo echo Eliminando acceso directo del escritorio...>>uninstall.bat
echo del /Q "%userprofile%\Desktop\Programador Errante.lnk">>uninstall.bat
echo echo.>>uninstall.bat
echo pause>>uninstall.bat
echo del /Q "%~dp0uninstall.bat" >> uninstall.bat
echo.
pause
