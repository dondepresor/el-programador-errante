package com.tracatra.wd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.tracatra.wd.model.Profile;

public class ProfileManager {

	private static final String LIST_ADAPTER_NAMES_CMD = "wmic nic get NetConnectionID";

	// "netsh interface ip set address \"Ethernet0\" static 192.168.1.10 255.255.255.0 192.168.1.1"
	private static final String[] SET_MANUAL_IP_SETTINGS_CMD = new String[] {"netsh", "interface", "ip", "set", "address", null, "static", null, null, null};

	// "netsh interface ip set address "Network Interface Name" dhcp"
	private static final String[] SET_DHCP_IP_SETTINGS_CMD = new String[] {"netsh", "interface", "ip", "set", "address", null, "source=dhcp"};

	// "netsh interface ip set dnsservers "Ethernet 2" static 1.1.1.1 primary validate=no"
	private static final String[] SET_MANUAL_PRIMARY_DNS_SETTINGS_CMD = new String[] {"netsh", "interface", "ip", "set", "dnsservers", null, "static", null, "primary", "validate=no"};

	// netsh interface ip add dnsservers "Ethernet 2" 1.1.1.1 validate=no
	private static final String[] SET_MANUAL_SECONDARY_DNS_SETTINGS_CMD = new String[] {"netsh", "interface", "ip", "add", "dnsservers", null, null, "validate=no"};

	// "netsh interface ip set dnsservers "Network Interface Name" source=dhcp"
	private static final String[] SET_DHCP_DNS_SETTINGS_CMD = new String[] {"netsh", "interface", "ip", "set", "dnsservers", null, "source=dhcp"};

	// REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /f /v ProxyEnable /t REG_DWORD /d 1
	private static final String[] SET_PROXY_SETTINGS_ENABLED_CMD = new String[] {"reg", "add", "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", "/f", "/v", "ProxyEnable", "/t", "REG_DWORD", "/d", null};

	// REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /f /v ProxyServer /t REG_SZ /d proxy:8080
	private static final String[] SET_PROXY_URL_CMD = new String[] {"reg", "add", "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", "/f", "/v", "ProxyServer", "/t", "REG_SZ", "/d", null};

	// REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /f /v ProxyOverride /t REG_SZ /d "localhost; 127.*"
	private static final String[] SET_PROXY_EXCEPTIONS_CMD = new String[] {"reg", "add", "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", "/f", "/v", "ProxyOverride", "/t", "REG_SZ", "/d", null};

	public static List<String> getAdapterNames() {
		List<String> result = new ArrayList<String>();
		try {
			Process p = Runtime.getRuntime().exec(LIST_ADAPTER_NAMES_CMD);
			p.waitFor();
			InputStream output = p.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(output, Charset.forName("Cp850")));
			String line = null;
			while((line = reader.readLine()) != null) {
				if(!line.trim().isEmpty()) result.add(line.trim());
			}
			result.remove(0);
		} catch(Exception e) {}
		return result;
	}

	public static List<ProcessBuilder> getProfileCommandList(Profile profile) throws IOException {
		List<ProcessBuilder> result = new ArrayList<>();
		if(profile.isChangeIPSettings()) {
			if(profile.isDhcpEnabled()) {
				result.add(setDhcpIpSettings(profile));
			} else {
				result.add(setManualIpSettings(profile));
			}
			if(profile.isManualDns()) {
				if(hasValue(profile.getPrimaryDns())) {
					result.add(setPrimaryDnsSettings(profile));
				}
				if(profile.getAdditionalDns()!=null && !profile.getAdditionalDns().isEmpty()) {
					result.add(setSecondaryDnsSettings(profile));
				}
			} else {
				result.add(setDhcpDnsSettings(profile));
			}
		}
		if(profile.isChangeProxySettings()) {
			result.addAll(setProxySettings(profile));
		}
		return result;
	}

	private static ProcessBuilder setManualIpSettings(Profile profile) throws IOException {
		String[] command = new String[SET_MANUAL_IP_SETTINGS_CMD.length];
		System.arraycopy(SET_MANUAL_IP_SETTINGS_CMD, 0, command, 0, SET_MANUAL_IP_SETTINGS_CMD.length);
		command[5] = "name=\""+ profile.getAdapterName() +"\"";
		command[7] = profile.getHostAddress();
		command[8] = profile.getNetMask();
		command[9] = profile.getDefaultGateway();
		return new ProcessBuilder(command);
	}

	private static ProcessBuilder setDhcpIpSettings(Profile profile) throws IOException {
		String[] command = new String[SET_DHCP_IP_SETTINGS_CMD.length];
		System.arraycopy(SET_DHCP_IP_SETTINGS_CMD, 0, command, 0, SET_DHCP_IP_SETTINGS_CMD.length);
		command[5] = "name=\""+ profile.getAdapterName() +"\"";
		return new ProcessBuilder(command);
	}

	private static ProcessBuilder setPrimaryDnsSettings(Profile profile) {
		String[] command = new String[SET_MANUAL_PRIMARY_DNS_SETTINGS_CMD.length];
		System.arraycopy(SET_MANUAL_PRIMARY_DNS_SETTINGS_CMD, 0, command, 0, SET_MANUAL_PRIMARY_DNS_SETTINGS_CMD.length);
		command[5] = "name=\""+ profile.getAdapterName() +"\"";
		command[7] = profile.getPrimaryDns();
		return new ProcessBuilder(command);
	}

	private static ProcessBuilder setSecondaryDnsSettings(Profile profile) {
		String[] command = new String[SET_MANUAL_SECONDARY_DNS_SETTINGS_CMD.length];
		System.arraycopy(SET_MANUAL_SECONDARY_DNS_SETTINGS_CMD, 0, command, 0, SET_MANUAL_SECONDARY_DNS_SETTINGS_CMD.length);
		command[5] = "name=\""+ profile.getAdapterName() +"\"";
		command[6] = profile.getAdditionalDns().get(0);
		return new ProcessBuilder(command);
	}

	private static ProcessBuilder setDhcpDnsSettings(Profile profile) {
		String[] command = new String[SET_DHCP_DNS_SETTINGS_CMD.length];
		System.arraycopy(SET_DHCP_DNS_SETTINGS_CMD, 0, command, 0, SET_DHCP_DNS_SETTINGS_CMD.length);
		command[5] = "name=\""+ profile.getAdapterName() +"\"";
		return new ProcessBuilder(command);
	}

	private static List<ProcessBuilder> setProxySettings(Profile profile) {
		List<ProcessBuilder> result = new ArrayList<>();
		Integer proxEnabledValue = profile.isProxyEnabled()? 1: 0;
		String[] setProxyEnabledCommand = new String[SET_PROXY_SETTINGS_ENABLED_CMD.length];
		System.arraycopy(SET_PROXY_SETTINGS_ENABLED_CMD, 0, setProxyEnabledCommand, 0, SET_PROXY_SETTINGS_ENABLED_CMD.length);
		setProxyEnabledCommand[9] = proxEnabledValue.toString();
		result.add(new ProcessBuilder(setProxyEnabledCommand));
		
		if(profile.isProxyEnabled()) {
			String[] setProxyUrlCommand = new String[SET_PROXY_URL_CMD.length];
			System.arraycopy(SET_PROXY_URL_CMD, 0, setProxyUrlCommand, 0, SET_PROXY_URL_CMD.length);
			setProxyUrlCommand[9] = profile.getProxyHost() +":"+ profile.getProxyPort().toString();
			result.add(new ProcessBuilder(setProxyUrlCommand));
			
			if(hasValue(profile.getProxyExceptions())) {
				String[] setProxyExceptionsCommand = new String[SET_PROXY_EXCEPTIONS_CMD.length];
				System.arraycopy(SET_PROXY_EXCEPTIONS_CMD, 0, setProxyExceptionsCommand, 0, SET_PROXY_EXCEPTIONS_CMD.length);
				setProxyExceptionsCommand[9] = profile.getProxyExceptions();
				result.add(new ProcessBuilder(setProxyExceptionsCommand));
			}
		}
		return result;
	}

	private static boolean hasValue(String data) {
		return data!=null && !data.trim().isEmpty();
	}
}
