package com.tracatra.wd;

import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.config.ConfigManager;
import com.tracatra.wd.ui.UiManager;

public class Main {

	private static final Logger LOG = LogManager.getLogger(Main.class);

	private static final ResourceBundle MESSAGES = ResourceBundle.getBundle("com.tracatra.wd.i18n", Locale.getDefault());

	public static void main(String[] args) {
		try {
			new UiManager();
		} catch(Exception e) {
			LOG.error("Error starting application: "+ (e==null? "null": e.getMessage()), e);
		}
	}

	public static String getLiteral(String key) {
		if(LOG.isDebugEnabled()) LOG.debug("[getLiteral](key: "+ key +")");
		return MESSAGES.getString(key);
	}

	public static File getApplicationFolder() throws Exception {
		LOG.debug("[getApplicationFolder]");
		String overridenPath = System.getProperty("wd.applicationFolder");
		return (overridenPath!=null)? new File(overridenPath) : new File(ConfigManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
	}
}
