package com.tracatra.wd.model;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

	private static final long serialVersionUID = 2794112804498533970L;

	private String alias;

	private boolean enabled;

	private boolean active;

	private String adapterName;

	private boolean changeIPSettings;

	private boolean dhcpEnabled;

	private String hostAddress;

	private String netMask;

	private String defaultGateway;

	private boolean manualDns;

	private String primaryDns;

	private List<String> additionalDns;

	private boolean changeProxySettings;

	private boolean proxyEnabled;

	private String proxyHost;

	private Integer proxyPort;

	private String proxyExceptions;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAdapterName() {
		return adapterName;
	}

	public void setAdapterName(String adapterName) {
		this.adapterName = adapterName;
	}

	public boolean isChangeIPSettings() {
		return changeIPSettings;
	}

	public void setChangeIPSettings(boolean changeIPSettings) {
		this.changeIPSettings = changeIPSettings;
	}

	public boolean isDhcpEnabled() {
		return dhcpEnabled;
	}

	public void setDhcpEnabled(boolean dhcpEnabled) {
		this.dhcpEnabled = dhcpEnabled;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getNetMask() {
		return netMask;
	}

	public void setNetMask(String netMask) {
		this.netMask = netMask;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public void setDefaultGateway(String defaultGateway) {
		this.defaultGateway = defaultGateway;
	}

	public boolean isManualDns() {
		return manualDns;
	}

	public void setManualDns(boolean manualDns) {
		this.manualDns = manualDns;
	}

	public String getPrimaryDns() {
		return primaryDns;
	}

	public void setPrimaryDns(String primaryDns) {
		this.primaryDns = primaryDns;
	}

	public List<String> getAdditionalDns() {
		return additionalDns;
	}

	public void setAdditionalDns(List<String> additionalDns) {
		this.additionalDns = additionalDns;
	}

	public boolean isChangeProxySettings() {
		return changeProxySettings;
	}

	public void setChangeProxySettings(boolean changeProxySettings) {
		this.changeProxySettings = changeProxySettings;
	}

	public boolean isProxyEnabled() {
		return proxyEnabled;
	}

	public void setProxyEnabled(boolean proxyEnabled) {
		this.proxyEnabled = proxyEnabled;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public Integer getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(Integer proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getProxyExceptions() {
		return proxyExceptions;
	}

	public void setProxyExceptions(String proxyExceptions) {
		this.proxyExceptions = proxyExceptions;
	}

	@Override
	public String toString() {
		return "Profile [alias=" + alias + ", enabled=" + enabled + ", active=" + active + ", adapterName="
				+ adapterName + ", changeIPSettings=" + changeIPSettings + ", dhcpEnabled=" + dhcpEnabled
				+ ", hostAddress=" + hostAddress + ", netMask=" + netMask + ", defaultGateway=" + defaultGateway
				+ ", manualDns=" + manualDns + ", primaryDns=" + primaryDns + ", additionalDns=" + additionalDns
				+ ", changeProxySettings=" + changeProxySettings + ", proxyEnabled=" + proxyEnabled + ", proxyHost="
				+ proxyHost + ", proxyPort=" + proxyPort + ", proxyExceptions=" + proxyExceptions + "]";
	}

}
