package com.tracatra.wd.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tracatra.wd.model.Profile;

public class Configuration implements Serializable {

	private static final long serialVersionUID = 1175728494264284281L;

	private Boolean startMinimized;

	private Boolean confirmOnExit;

	private List<Profile> profiles = new ArrayList<Profile>();

	public Boolean getConfirmOnExit() {
		return confirmOnExit;
	}

	public void setConfirmOnExit(Boolean confirmOnExit) {
		this.confirmOnExit = confirmOnExit;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public void addProfile(Profile profile) {
		this.profiles.add(profile);
	}

	public Boolean getStartMinimized() {
		return startMinimized;
	}

	public void setStartMinimized(Boolean startMinimized) {
		this.startMinimized = startMinimized;
	}

	@Override
	public String toString() {
		return "Configuration [startMinimized=" + startMinimized + ", confirmOnExit=" + confirmOnExit + ", profiles="
				+ profiles + "]";
	}
}
