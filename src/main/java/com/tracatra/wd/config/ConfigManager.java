package com.tracatra.wd.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.Main;
import com.tracatra.wd.model.Profile;

public class ConfigManager {

	private static final Logger log = LogManager.getLogger(ConfigManager.class);

	private static final String CONFIG_FILE_NAME = "config.properties";

	private static final String START_MINIFIED = "startMinified";
	private static final String CONFIRM_ON_EXIT = "confirmOnExit";
	
	private static final String PROFILES = "profiles";
	private static final String PROFILE_PREFIX = "profile.";
	private static final String PROFILE_ENABLED_SUFFIX = ".enabled";
	private static final String PROFILE_ADAPTER_NAME_SUFFIX = ".adapterName";
	private static final String PROFILE_CHANGE_IP_SETTINGS_SUFFIX = ".changeIpSettings";
	private static final String PROFILE_DHCP_ENABLED_SUFFIX = ".dhcpEnabled";
	private static final String PROFILE_HOST_ADDRESS_SUFFIX = ".hostAddress";
	private static final String PROFILE_NETMASK_SUFFIX = ".netmask";
	private static final String PROFILE_DEFAULT_GATEWAY_SUFFIX = ".defaultGateway";
	private static final String PROFILE_MANUAL_DNS_SUFFIX = ".namualDns";
	private static final String PROFILE_PRIMARY_DNS_SUFFIX = ".primaryDns";
	private static final String PROFILE_ADDITIONAL_DNS_LIST_SUFFIX = ".additionalDnsList";
	private static final String PROFILE_CHANGE_PROXY_SETTINGS_SUFFIX = ".changeProxySettings";
	private static final String PROFILE_PROXY_ENABLED_SUFFIX = ".proxyEnabled";
	private static final String PROFILE_PROXY_HOST_SUFFIX = ".proxyHost";
	private static final String PROFILE_PROXY_PORT_SUFFIX = ".proxyPort";
	private static final String PROFILE_PROXY_EXCEPTIONS_SUFFIX = ".proxyExceptions";

	public static Configuration loadConfiguration() {
		log.debug("[loadConfiguration]");
		Configuration result = new Configuration();
		Properties properties = new Properties();
		try {
			File configFile = new File(Main.getApplicationFolder(), CONFIG_FILE_NAME);
			if(configFile.exists()) {
				try(InputStream fileInputStream = new FileInputStream(configFile)) {
					properties.load(fileInputStream);
					result = parse(properties);
				}
			} else {
				result.setConfirmOnExit(true);
				result.setStartMinimized(false);
			}
		} catch(Exception e) {
			log.warn("Error loading configuration: "+ (e==null? "null": e.getMessage()), e);
		}
		if(log.isDebugEnabled()) log.debug("Config loaded: "+ result);
		return result;
	}

	public static void saveConfiguration(Configuration config) {
		if(log.isDebugEnabled()) log.debug("[saveConfiguration](config: "+ config +")");
		Properties properties = format(config);
		FileOutputStream outputStream = null;
		try {
			File configFile = new File(Main.getApplicationFolder(), CONFIG_FILE_NAME);
			if(log.isDebugEnabled()) log.debug("[saveConfiguration] Saving to file: "+ configFile.getCanonicalPath());
			if(!configFile.exists()) {
				if(!configFile.createNewFile()) throw new Exception("Config file could not be created");
			}
			outputStream = new FileOutputStream(configFile);
			properties.store(outputStream, null);
		} catch(Exception e) {
			log.warn("Error saving configuration: "+ (e==null? "null": e.getMessage()), e);
			if(outputStream!=null) {
				try {
					outputStream.close();
				} catch(Exception ex) {}
				outputStream = null;
			}
		}
	}

	private static Configuration parse(Properties properties) {
		if(log.isDebugEnabled()) log.debug("[parse](properties? "+ (properties!=null) +")");
		Configuration result = null;
		if(properties!=null) {
			result = new Configuration();
			result.setStartMinimized(Boolean.parseBoolean(properties.getProperty(START_MINIFIED, "false")));
			result.setConfirmOnExit(Boolean.parseBoolean(properties.getProperty(CONFIRM_ON_EXIT, "true")));
			String profilesList = properties.getProperty(PROFILES, "").trim();
			List<String> profilesAlias = Arrays.asList(profilesList.isEmpty()? new String[]{}: profilesList.split(","));
			for(String alias : profilesAlias) {
				result.addProfile(parseProfile(alias, properties));
			}
		}
		return result;
	}

	private static Profile parseProfile(String alias, Properties properties) {
		Profile profile = new Profile();
		profile.setAlias(alias);
		profile.setEnabled(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_ENABLED_SUFFIX, "false")));
		profile.setAdapterName(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_ADAPTER_NAME_SUFFIX));
		profile.setChangeIPSettings(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_CHANGE_IP_SETTINGS_SUFFIX, "false")));
		profile.setDhcpEnabled(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_DHCP_ENABLED_SUFFIX, "true")));
		profile.setHostAddress(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_HOST_ADDRESS_SUFFIX));
		profile.setNetMask(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_NETMASK_SUFFIX));
		profile.setDefaultGateway(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_DEFAULT_GATEWAY_SUFFIX));
		profile.setManualDns(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_MANUAL_DNS_SUFFIX, "false")));
		profile.setPrimaryDns(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_PRIMARY_DNS_SUFFIX));
		profile.setAdditionalDns(Arrays.asList(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_ADDITIONAL_DNS_LIST_SUFFIX, "").split(",")));
		profile.setChangeProxySettings(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_CHANGE_PROXY_SETTINGS_SUFFIX, "false")));
		profile.setProxyEnabled(Boolean.parseBoolean(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_ENABLED_SUFFIX, "false")));
		profile.setProxyHost(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_HOST_SUFFIX));
		String proxyPortStr = properties.getProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_PORT_SUFFIX);
		if(proxyPortStr!=null) {
			try {
				profile.setProxyPort(Integer.parseInt(proxyPortStr));
			} catch(NumberFormatException e) {}
		}
		profile.setProxyExceptions(properties.getProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_EXCEPTIONS_SUFFIX));
		return profile;
	}

	private static Properties format(Configuration config) {
		if(log.isDebugEnabled()) log.debug("[format](config? "+ (config!=null) +")");
		Properties result = new Properties();
		if(config!=null) {
			result.setProperty(START_MINIFIED, config.getStartMinimized().toString());
			result.setProperty(CONFIRM_ON_EXIT, config.getConfirmOnExit().toString());
			String profilesAlias = config.getProfiles().stream().map(profile->profile.getAlias()).collect(Collectors.joining(","));
			result.setProperty(PROFILES, profilesAlias);
			for(Profile profile : config.getProfiles()) {
				formatProfile(profile, result);
			}
		}
		return result;
	}

	private static void formatProfile(Profile profile, Properties properties) {
		String alias = profile.getAlias();
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_ENABLED_SUFFIX, Boolean.toString(profile.isEnabled()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_ADAPTER_NAME_SUFFIX, profile.getAdapterName());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_CHANGE_IP_SETTINGS_SUFFIX, Boolean.toString(profile.isChangeIPSettings()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_DHCP_ENABLED_SUFFIX, Boolean.toString(profile.isDhcpEnabled()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_HOST_ADDRESS_SUFFIX, profile.getHostAddress());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_NETMASK_SUFFIX, profile.getNetMask());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_DEFAULT_GATEWAY_SUFFIX, profile.getDefaultGateway());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_MANUAL_DNS_SUFFIX, Boolean.toString(profile.isManualDns()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_PRIMARY_DNS_SUFFIX, profile.getPrimaryDns());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_ADDITIONAL_DNS_LIST_SUFFIX, profile.getAdditionalDns().stream().collect(Collectors.joining(",")));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_CHANGE_PROXY_SETTINGS_SUFFIX, Boolean.toString(profile.isChangeProxySettings()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_ENABLED_SUFFIX, Boolean.toString(profile.isProxyEnabled()));
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_HOST_SUFFIX, profile.getProxyHost());
		if(profile.getProxyPort()!=null)
			properties.setProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_PORT_SUFFIX, profile.getProxyPort().toString());
		properties.setProperty(PROFILE_PREFIX + alias + PROFILE_PROXY_EXCEPTIONS_SUFFIX, profile.getProxyExceptions());
	}
}
