package com.tracatra.wd.ui;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.Main;
import com.tracatra.wd.config.ConfigManager;
import com.tracatra.wd.config.Configuration;
import com.tracatra.wd.model.Profile;
import com.tracatra.wd.ui.components.ProfileSelectedActionListener;
import com.tracatra.wd.ui.threading.Invocable;
import com.tracatra.wd.ui.threading.ParametrizedInvocable;
import com.tracatra.wd.ui.threading.SwingInvoker;

public class UiManager {

	private static final Logger log = LogManager.getLogger(UiManager.class);

	private boolean systraySupported;

	private MainFrame mainFrame;
	private SwingInvoker invoker;
	private SystemTray systemTray;
	private TrayIcon trayIcon;

	private Configuration config;

	public UiManager() throws Exception {
		log.debug("ui manager");
		this.config = ConfigManager.loadConfiguration();
		
		this.invoker = new SwingInvoker(this);
		this.mainFrame = new MainFrame(this);
		this.systraySupported = SystemTray.isSupported();
		
		if(!systraySupported || !config.getStartMinimized()) {
			this.mainFrame.showFrame();
		}
		if(systraySupported) {
			initSystemTray();
		}
	}

	public void invoke(Invocable invocable) {
		this.invoker.runSwing(invocable);
	}

	public void invoke(ParametrizedInvocable invocable, Object params) {
		this.invoker.runSwing(invocable, params);
	}

	public Configuration getConfig() {
		return this.config;
	}

	public MainFrame getMainFrame() {
		return mainFrame;
	}

	public void mainWindowCloseRequested() {
		log.debug("[mainWindowCloseRequested]");
		if(!systraySupported) {
			this.controlledExit(true);
		}
	}

	public void mainWindowExitRequested(boolean save) {
		log.debug("[mainWindowExitRequested]");
		this.controlledExit(save);
	}

	public void launchProfile(String profileAlias) {
		log.debug("[launchProfile] profileAlias: "+ profileAlias);
		if(profileAlias!=null) {
			if(mainFrame.isStarted()) {
				mainFrame.updateConfiguration();
			}
			for(Profile profile : config.getProfiles()) {
				if(profileAlias.equals(profile.getAlias())) {
					invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							new ProfileExecutionFrame(manager, profile);
						}
					});
					break;
				}
			}
		}
	}

	public boolean isSystraySupported() {
		return systraySupported;
	}

	private void initSystemTray() throws Exception {
		log.debug("[initSystemTray]");
		
		ImageIcon icon = new ImageIcon(getClass().getResource("icons/icon.png"));
		trayIcon = new TrayIcon(icon.getImage(), Main.getLiteral("mainTitle"));
		trayIcon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						mainFrame.showFrame();
					}
				});
			}
		});
		trayIcon.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					refreshTrayIconMenu();
				}
			}
		});
		systemTray = SystemTray.getSystemTray();
		systemTray.add(trayIcon);
	}

	private void refreshTrayIconMenu() {
		log.debug("[refreshTrayIconMenu]");
		
		PopupMenu menu = new PopupMenu();
		MenuItem openWindowItem = new MenuItem(Main.getLiteral("popupMenuOpen"));
		openWindowItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						mainFrame.showFrame();
					}
				});
			}
		});
		menu.add(openWindowItem);
		menu.addSeparator();
		if(mainFrame.isStarted()) {
			mainFrame.updateConfiguration();
		}
		for(Profile profile : config.getProfiles()) {
			if(profile.isEnabled()) {
				MenuItem profileItem = new MenuItem(profile.getAlias());
				profileItem.addActionListener(new ProfileSelectedActionListener(this, profile.getAlias()));
				menu.add(profileItem);
			}
		}
		menu.addSeparator();
		MenuItem exitItem = new MenuItem(Main.getLiteral("popupMenuExit"));
		exitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controlledExit(true);
			}
		});
		menu.add(exitItem);
		MenuItem exitWithoutSavingItem = new MenuItem(Main.getLiteral("popupMenuExitWithoutSaving"));
		exitWithoutSavingItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controlledExit(false);
			}
		});
		menu.add(exitWithoutSavingItem);
		
		trayIcon.setPopupMenu(menu);
	}

	private void controlledExit(boolean save) {
		log.debug("[controlledExit]");
		if(config.getConfirmOnExit()) {
			String ObjButtons[] = {
				Main.getLiteral("yes"),
				Main.getLiteral("no")
			};
			int PromptResult = JOptionPane.showOptionDialog(null, 
				Main.getLiteral("confirmExitText"), 
				Main.getLiteral("confirmExitTitle"), 
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.WARNING_MESSAGE, 
				null, 
				ObjButtons,
				ObjButtons[1]
			);
			if(PromptResult==0) {
				if(save && mainFrame.isStarted()) {
					mainFrame.updateConfiguration();
					ConfigManager.saveConfiguration(this.config);
				}
				System.exit(0);
			}
		} else {
			if(save && mainFrame.isStarted()) {
				mainFrame.updateConfiguration();
				ConfigManager.saveConfiguration(this.config);
			}
			System.exit(0);
		}
	}
}
