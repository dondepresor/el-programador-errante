package com.tracatra.wd.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.Main;
import com.tracatra.wd.model.Profile;
import com.tracatra.wd.ui.components.SortableTabbedPane;
import com.tracatra.wd.ui.components.TabsReorderedEvent;
import com.tracatra.wd.ui.components.TabsReorderedListener;
import com.tracatra.wd.ui.threading.Invocable;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -8393862505360859971L;

	private static final Logger LOG = LogManager.getLogger(MainFrame.class);

	private static final Dimension WINDOW_SIZE = new Dimension(400, 600);

	private UiManager manager;
	private boolean started = false;

	private JMenuBar menuBar;
	private JCheckBoxMenuItem startMinimizedCheckBoxMenuItem;
	private JCheckBoxMenuItem confirmOnExitCheckboxMenuItem;
	private JMenuItem minimizeMenuItem;
	private SortableTabbedPane tabContainer;
	private JLabel statusTextLabel;
	private Action addProfileAction;
	private Action cloneProfileAction;
	private Action deleteProfileAction;
	private Action runProfileAction;

	public MainFrame(UiManager manager) {
		this.manager = manager;
	}

	public UiManager getManager() {
		return manager;
	}

	public void updateTabTitle(int index, String newTitle) {
		tabContainer.setTitleAt(index, newTitle);
	}

	public void updateConfiguration() {
		List<Profile> profiles = new ArrayList<Profile>();
		for(int c=0; c<tabContainer.getTabCount(); c++) {
			ProfilePanel panel = (ProfilePanel)tabContainer.getComponentAt(c);
			profiles.add(panel.saveValues());
		}
		manager.getConfig().setProfiles(profiles);
	}

	public void showFrame() {
		this.setVisible(true);
		if(!this.started) {
			this.init();
		}
	}

	public boolean isStarted() {
		return started;
	}

	public void updateComponentStatus() {
		boolean selectedProfile = tabContainer.getSelectedIndex()>-1;
		cloneProfileAction.setEnabled(selectedProfile);
		deleteProfileAction.setEnabled(selectedProfile);
		runProfileAction.setEnabled(selectedProfile && getCurrentProfile().isEnabled());
	}


	private void init() {
		LOG.debug("[init]");
		manager.invoke(new Invocable() {
			@Override
			public void run(UiManager manager) {
				initWindow();
				initActions();
				initMenuBar();
				
				Container c = getContentPane();
				c.setLayout(new BorderLayout());
				c.add(initToolbar(), BorderLayout.NORTH);
				c.add(initStatusBar(), BorderLayout.SOUTH);
				c.add(initTabContainer(), BorderLayout.CENTER);
				
				updateComponentStatus();
				for(Profile profile : manager.getConfig().getProfiles()) {
					addProfilePanel(profile);
				}
				if(tabContainer.getSelectedIndex()>-1) {
					tabContainer.setSelectedIndex(0);
				}
				
				statusTextLabel.setText(Main.getLiteral("applicationReady"));
				started = true;
			}
		});
	}

	private void initWindow() {
		LOG.debug("[initWindow]");
		this.setTitle(Main.getLiteral("mainTitle"));
		this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				manager.mainWindowCloseRequested();
			}
		});
		this.setResizable(false);
		this.setSize(WINDOW_SIZE);
		this.setLocationRelativeTo(null);
		ImageIcon icon = new ImageIcon(getClass().getResource("icons/icon.png"));
		setIconImage(icon.getImage());
	}

	private void initActions() {
		LOG.debug("[initActions]");
		addProfileAction = new AbstractAction() {
			private static final long serialVersionUID = -7194414734525781417L;
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[addProfileAction.actionPerformed]");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						addProfilePanel(null);
					}
				});
			}
		};
		addProfileAction.putValue(Action.NAME, Main.getLiteral("menuProfileAdd"));
		addProfileAction.putValue(Action.SHORT_DESCRIPTION, Main.getLiteral("menuProfileAddDescription"));
		addProfileAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(Main.getLiteral("menuProfileAddMnemonic").charAt(0)));
		Icon plusIcon = new ImageIcon(getClass().getResource("icons/plus.png"));
		addProfileAction.putValue(Action.SMALL_ICON, plusIcon);
		
		cloneProfileAction = new AbstractAction() {
			private static final long serialVersionUID = -3226274418522626636L;
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[cloneProfileAction.actionPerformed]");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						cloneCurrentPanel();
					}
				});
			}
		};
		cloneProfileAction.putValue(Action.NAME, Main.getLiteral("menuProfileDuplicate"));
		cloneProfileAction.putValue(Action.SHORT_DESCRIPTION, Main.getLiteral("menuProfileDuplicateDescription"));
		cloneProfileAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(Main.getLiteral("menuProfileDuplicateMnemonic").charAt(0)));
		Icon cloneIcon = new ImageIcon(getClass().getResource("icons/clone.png"));
		cloneProfileAction.putValue(Action.SMALL_ICON, cloneIcon);
		
		deleteProfileAction = new AbstractAction() {
			private static final long serialVersionUID = 636595204354162277L;
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[deleteProfileAction.actionPerformed]");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						deleteCurrentPanel();
					}
				});
			}
		};
		deleteProfileAction.putValue(Action.NAME, Main.getLiteral("menuProfileDelete"));
		deleteProfileAction.putValue(Action.SHORT_DESCRIPTION, Main.getLiteral("menuProfileDeleteDescription"));
		deleteProfileAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(Main.getLiteral("menuProfileDeleteMnemonic").charAt(0)));
		Icon crossIcon = new ImageIcon(getClass().getResource("icons/cross.png"));
		deleteProfileAction.putValue(Action.SMALL_ICON, crossIcon);
		
		runProfileAction = new AbstractAction() {
			private static final long serialVersionUID = 3342790751535422615L;
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[runProfileAction.actionPerformed]");
				final Profile currentProfile = getCurrentProfile();
				if(currentProfile!=null) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							manager.launchProfile(currentProfile.getAlias());
						}
					});
				}
			}
		};
		runProfileAction.putValue(Action.NAME, Main.getLiteral("menuProfileRun"));
		runProfileAction.putValue(Action.SHORT_DESCRIPTION, Main.getLiteral("menuProfileRunDescription"));
		runProfileAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(Main.getLiteral("menuProfileRunMnemonic").charAt(0)));
		Icon playIcon = new ImageIcon(getClass().getResource("icons/play.png"));
		runProfileAction.putValue(Action.SMALL_ICON, playIcon);
	}

	private void initMenuBar() {
		LOG.debug("[initMenuBar]");
		menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu(Main.getLiteral("menuFile"));
		fileMenu.setMnemonic(Main.getLiteral("menuFileMnemonic").charAt(0));
		
		startMinimizedCheckBoxMenuItem = new JCheckBoxMenuItem(Main.getLiteral("menuFileStartMinimized"));
		startMinimizedCheckBoxMenuItem.setMnemonic(Main.getLiteral("menuFileStartMinimizedMnemonic").charAt(0));
		startMinimizedCheckBoxMenuItem.setToolTipText(Main.getLiteral("menuFileStartMinimizedDescription"));
		startMinimizedCheckBoxMenuItem.setState(manager.getConfig().getStartMinimized());
		startMinimizedCheckBoxMenuItem.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				manager.getConfig().setStartMinimized(startMinimizedCheckBoxMenuItem.getState());
			}
		});
		fileMenu.add(startMinimizedCheckBoxMenuItem);
		confirmOnExitCheckboxMenuItem = new JCheckBoxMenuItem(Main.getLiteral("menuFileConfirm"));
		confirmOnExitCheckboxMenuItem.setMnemonic(Main.getLiteral("menuFileConfirmMnemonic").charAt(0));
		confirmOnExitCheckboxMenuItem.setToolTipText(Main.getLiteral("menuFileConfirmDescription"));
		confirmOnExitCheckboxMenuItem.setState(manager.getConfig().getConfirmOnExit());
		confirmOnExitCheckboxMenuItem.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				manager.getConfig().setConfirmOnExit(confirmOnExitCheckboxMenuItem.getState());
			}
		});
		fileMenu.add(confirmOnExitCheckboxMenuItem);
		fileMenu.addSeparator();
		minimizeMenuItem = new JMenuItem(Main.getLiteral("menuFileMinimize"));
		minimizeMenuItem.setMnemonic(Main.getLiteral("menuFileMinimizeMnemonic").charAt(0));
		minimizeMenuItem.setToolTipText(Main.getLiteral("menuFileMinimizeDescription"));
		minimizeMenuItem.setEnabled(manager.isSystraySupported());
		minimizeMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						setVisible(false);
					}
				});
			}
		});
		fileMenu.add(minimizeMenuItem);
		JMenuItem exitMenuItem = new JMenuItem(Main.getLiteral("menuFileExit"));
		exitMenuItem.setMnemonic(Main.getLiteral("menuFileExitMnemonic").charAt(0));
		exitMenuItem.setToolTipText(Main.getLiteral("menuFileExitDescription"));
		exitMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[actionPerformed]");
				manager.mainWindowExitRequested(true);
			}
		});
		fileMenu.add(exitMenuItem);
		JMenuItem exitWithoutSavingMenuItem = new JMenuItem(Main.getLiteral("menuFileExitWithoutSaving"));
		exitWithoutSavingMenuItem.setMnemonic(Main.getLiteral("menuFileExitWithoutSavingMnemonic").charAt(0));
		exitWithoutSavingMenuItem.setToolTipText(Main.getLiteral("menuFileExitWithoutSavingDescription"));
		exitWithoutSavingMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.debug("[actionPerformed]");
				manager.mainWindowExitRequested(false);
			}
		});
		fileMenu.add(exitWithoutSavingMenuItem);
		
		JMenu profileMenu = new JMenu(Main.getLiteral("menuProfile"));
		profileMenu.setMnemonic(Main.getLiteral("menuProfileMnemonic").charAt(0));
		
		JMenuItem addProfileItem = new JMenuItem(addProfileAction);
		profileMenu.add(addProfileItem);
		
		JMenuItem duplicateProfileItem = new JMenuItem(cloneProfileAction);
		profileMenu.add(duplicateProfileItem);
		
		JMenuItem deleteProfileItem = new JMenuItem(deleteProfileAction);
		profileMenu.add(deleteProfileItem);
		
		JMenuItem runProfileItem = new JMenuItem(runProfileAction);
		profileMenu.add(runProfileItem);
		
		menuBar.add(fileMenu);
		menuBar.add(profileMenu);
		this.setJMenuBar(menuBar);
	}

	private JComponent initToolbar() {
		LOG.debug("[initToolbar]");
		
		JPanel toolbar = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 2));
		JButton addButton = new JButton(addProfileAction);
		addButton.setHideActionText(true);
		addButton.setToolTipText((String)addProfileAction.getValue(Action.SHORT_DESCRIPTION));
		toolbar.add(addButton);
		
		JButton cloneButton = new JButton(cloneProfileAction);
		cloneButton.setHideActionText(true);
		toolbar.add(cloneButton);
		
		JButton deleteButton = new JButton(deleteProfileAction);
		deleteButton.setHideActionText(true);
		toolbar.add(deleteButton);
		
		JButton runButton = new JButton(runProfileAction);
		runButton.setHideActionText(true);
		toolbar.add(runButton);
		
		return toolbar;
	}

	private JComponent initTabContainer() {
		LOG.debug("[initTabContainer]");
		tabContainer = new SortableTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
		tabContainer.addTabsReorderedListener(new TabsReorderedListener() {
			@Override
			public void tabsReordered(TabsReorderedEvent e) {
				refreshTabIndices();
			}
		});
		tabContainer.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				LOG.debug("[tabContainer.stateChanged]");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						manager.getMainFrame().updateComponentStatus();
					}
				});
			}
		});
		return tabContainer;
	}

	private JComponent initStatusBar() {
		LOG.debug("[initStatusBar]");
		Border border = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		Border margin = new EmptyBorder(2, 10, 2, 10);
		Border toolbarLabelsBorder = new CompoundBorder(border, margin);
		
		statusTextLabel = new JLabel(" ", SwingConstants.RIGHT);
		statusTextLabel.setBorder(toolbarLabelsBorder);
		
		JPanel statusBar = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 2));
		statusBar.add(statusTextLabel);
		
		return statusBar;
	}

	private void addProfilePanel(Profile profile) {
		int newIndex = tabContainer.getTabCount();
		ProfilePanel panel = new ProfilePanel(manager, newIndex);
		tabContainer.addTab(Main.getLiteral("newProfileLabel"), panel);
		tabContainer.setSelectedIndex(newIndex);
		panel.completeInit();
		if(profile!=null) {
			manager.invoke(new Invocable() {
				@Override
				public void run(UiManager manager) {
					panel.loadValues(profile);
				}
			});
		}
	}

	private void cloneCurrentPanel() {
		Profile currentProfile = getCurrentProfile();
		if(currentProfile!=null) {
			addProfilePanel(currentProfile);
		}
	}

	private void deleteCurrentPanel() {
		int currentIndex = tabContainer.getSelectedIndex();
		if(currentIndex>-1) {
			tabContainer.remove(currentIndex);
			refreshTabIndices();
		}
	}

	private void refreshTabIndices() {
		for(int c=0; c<tabContainer.getTabCount(); c++) {
			ProfilePanel panel = (ProfilePanel)tabContainer.getComponentAt(c);
			panel.updateIndex(c);
		}
	}
	private Profile getCurrentProfile() {
		int currentIndex = tabContainer.getSelectedIndex();
		if(currentIndex>-1) {
			ProfilePanel panel = (ProfilePanel)tabContainer.getComponentAt(currentIndex);
			return panel.saveValues();
		}
		return null;
	}
}
