package com.tracatra.wd.ui.threading;

import com.tracatra.wd.ui.UiManager;

/**
 * Runnable-like inteface,
 * where the run() code can access
 * the <code>MainFrame</code> instance.
 * Also supports parameter passing.
 */
public interface ParametrizedInvocable {

	/**
	 * Code to run.
	 * @param manager the ui manager instancce.
	 * @parameter the method arguments.
	 */
	public void run(UiManager manager, Object parameter);
}
