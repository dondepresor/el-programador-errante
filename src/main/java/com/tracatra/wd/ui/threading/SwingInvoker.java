package com.tracatra.wd.ui.threading;

import com.tracatra.wd.ui.UiManager;

/**
 * Runs code in a swing safe way.
 *
 */
public class SwingInvoker {

	/** The main application frame. */
	private UiManager manager;

	/**
	 * Class constructor.
	 * @param manager the main application frame.
	 */
	public SwingInvoker(UiManager manager) {
		this.manager = manager;
	}
	
	/**
	 * Run a swing-safe code fragment.
	 * @param invocable the code to run.
	 */
	public void runSwing(Invocable invocable) {
		javax.swing.SwingUtilities.invokeLater(new InvocableRunnable(manager, invocable));
	}
	
	/**
	 * Run a swing-safe code fragment.
	 * @param invocable the code to run.
	 * @param parameter the parameter to pass.
	 */
	public void runSwing(ParametrizedInvocable invocable, Object parameter) {
		javax.swing.SwingUtilities.invokeLater(new InvocableRunnable(manager, invocable, parameter));
	}
	
	/**
	 * Private intermediate holder class for runnable code.
	 *
	 */
	private class InvocableRunnable implements Runnable {

		/** The action to run, when no parameters are specified. */
		private Invocable action;
		
		/** The action to run when there are parameters. */
		private ParametrizedInvocable parametrizedAction;
		
		/** The main application frame. */
		private UiManager manager;
		
		/** Parameter to pass. */
		private Object parameter;
		
		/**
		 * Constructor for unparametrized requests.
		 * @param manager the ui manager.
		 * @param action the action to run.
		 */
		public InvocableRunnable(UiManager manager, Invocable action) {
			this.manager = manager;
			this.action = action;
			this.parametrizedAction = null;
			this.parameter = null;
		}
		
		/**
		 * Constructor for parametrized requests.
		 * @param manager the ui manager.
		 * @param action the action to run.
		 * @param parameter the parameter to pass.
		 */
		public InvocableRunnable(UiManager manager, ParametrizedInvocable action, Object parameter) {
			this.manager = manager;
			this.parametrizedAction = action;
			this.action = null;
			this.parameter = parameter;
		}
		
		/**
		 * Runs the specified code.
		 */
		public void run() {
			if(this.action!=null) {
				this.action.run(this.manager);
			} else if(this.parametrizedAction!=null) {
				this.parametrizedAction.run(this.manager, this.parameter);
			}
		}
	}
}
