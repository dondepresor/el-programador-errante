package com.tracatra.wd.ui.threading;

import com.tracatra.wd.ui.UiManager;

/**
 * Runnable-like inteface,
 * where the run() code can access
 * the <code>UiManager</code> instance.
 *
 */
public interface Invocable {

	/**
	 * Code to run.
	 * @param manager the ui manager instancce.
	 */
	public void run(UiManager manager);
}
