package com.tracatra.wd.ui.components;

public interface TabsReorderedListener {
	public void tabsReordered(TabsReorderedEvent e);
}