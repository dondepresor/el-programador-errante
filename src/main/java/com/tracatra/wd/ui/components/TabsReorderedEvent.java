package com.tracatra.wd.ui.components;

public class TabsReorderedEvent {

	private final int from, to;

	public TabsReorderedEvent(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public int getFrom() {
		return from;
	}

	public int getTo() {
		return to;
	}
}