package com.tracatra.wd.ui.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.tracatra.wd.ui.UiManager;

public class ProfileSelectedActionListener implements ActionListener {

	private final UiManager uiManager;

	private final String profileName;

	public ProfileSelectedActionListener(UiManager uiManager, String profileName) {
		this.uiManager = uiManager;
		this.profileName = profileName;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		uiManager.launchProfile(profileName);
	}
}
