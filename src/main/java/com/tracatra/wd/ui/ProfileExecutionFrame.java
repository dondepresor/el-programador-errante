package com.tracatra.wd.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.Main;
import com.tracatra.wd.ProfileManager;
import com.tracatra.wd.model.Profile;
import com.tracatra.wd.ui.threading.Invocable;

public class ProfileExecutionFrame extends JFrame {

	private static final long serialVersionUID = -5607271025550515831L;

	private static final Logger LOG = LogManager.getLogger(ProfileExecutionFrame.class);

	private static final Dimension WINDOW_SIZE = new Dimension(640, 480);

	private UiManager manager;
	private Profile profile;
	private Action closeAction;
	private JButton closeButton;
	private JTextArea logTextArea;
	private boolean done = false;

	public ProfileExecutionFrame(UiManager manager, Profile profile) {
		LOG.debug("[ProfileExecutionFrame]");
		this.manager = manager;
		this.profile = profile;
		this.setVisible(true);
		this.init();
	}

	private void init() {
		LOG.debug("[init]");
		manager.invoke(new Invocable() {
			@Override
			public void run(UiManager manager) {
				initWindow();
				Container c = getContentPane();
				c.setLayout(new BorderLayout());
				c.add(initHeader(), BorderLayout.NORTH);
				c.add(initStatusBar(), BorderLayout.SOUTH);
				c.add(initLogPanel(), BorderLayout.CENTER);
				
				launchProcess();
			}
		});
	}

	private void launchProcess() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for(ProcessBuilder builder : ProfileManager.getProfileCommandList(profile)) {
						BufferedReader reader = null;
						try {
							String command = builder.command().stream().collect(Collectors.joining(" "));
							logTextArea.append(">"+ command + "\n");
							builder.redirectError();
							Process process = builder.start();
							reader = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.forName("Cp850")));
							process.waitFor();
							String line = null;
							while((line = reader.readLine())!=null) {
								logTextArea.append(line + "\n");
							}
						} catch(Exception e) {
							for(StackTraceElement line : e.getStackTrace()) {
								logTextArea.append(line + "\n");
							}
						} finally {
							if(reader!=null) {
								try {
									reader.close();
								} catch(Exception e) {}
							}
						}
					}
				} catch(Exception e) {
					for(StackTraceElement line : e.getStackTrace()) {
						logTextArea.append(line + "\n");
					}
				} finally {
					done = true;
					closeAction.setEnabled(true);
				}
			}
		}).start();
	}

	private void initWindow() {
		LOG.debug("[initWindow]");
		this.setTitle(Main.getLiteral("profileExecutionTitle"));
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				closeFrame();
			}
		});
		this.setResizable(false);
		this.setSize(WINDOW_SIZE);
		this.setLocationRelativeTo(null);
		ImageIcon icon = new ImageIcon(getClass().getResource("icons/icon.png"));
		setIconImage(icon.getImage());
	}

	private Component initHeader() {
		JPanel result = new JPanel();
		result.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel(Main.getLiteral("runningProfileLabel") +" "+ profile.getAlias());
		label.setAlignmentX(0);
		result.add(label);
		return result;
	}

	private Component initLogPanel() {
		JPanel result = new JPanel();
		result.setLayout(new BorderLayout(5,  5));
		result.add(new JPanel(), BorderLayout.NORTH);
		result.add(new JPanel(), BorderLayout.SOUTH);
		result.add(new JPanel(), BorderLayout.EAST);
		result.add(new JPanel(), BorderLayout.WEST);
		logTextArea = new JTextArea();
		logTextArea.setAlignmentX(0);
		logTextArea.setEditable(false);
		logTextArea.setBackground(Color.BLACK);
		logTextArea.setForeground(Color.WHITE);
		logTextArea.setFont(Font.decode(Font.MONOSPACED));
		logTextArea.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(logTextArea, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		result.add(scrollPane, BorderLayout.CENTER);
		return result;
	}

	private Component initStatusBar() {
		JPanel result = new JPanel();
		result.setLayout(new FlowLayout(FlowLayout.RIGHT));
		closeAction = new AbstractAction() {
			private static final long serialVersionUID = 441275874293842356L;
			@Override
			public void actionPerformed(ActionEvent e) {
				closeFrame();
			}
		};
		closeAction.putValue(Action.NAME, Main.getLiteral("closeExecution"));
		closeAction.putValue(Action.SHORT_DESCRIPTION, Main.getLiteral("closeExecutionDescription"));
		closeAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(Main.getLiteral("closeExecutionMnemonic").charAt(0)));
		closeAction.setEnabled(false); 
		closeButton = new JButton(closeAction);
		result.add(closeButton);
		return result;
	}

	private void closeFrame() {
		if(done) {
			setVisible(false);
			dispose();
		}
	}
}
