package com.tracatra.wd.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracatra.wd.Main;
import com.tracatra.wd.ProfileManager;
import com.tracatra.wd.model.Profile;
import com.tracatra.wd.ui.threading.Invocable;

public class ProfilePanel extends JScrollPane {

	private static final long serialVersionUID = 2114604596573642667L;

	private static final Logger log = LogManager.getLogger(ProfilePanel.class);

	private UiManager manager;
	private int index;
	private boolean disableTriggers = false;

	private Action enabledAction;
	private Action changeIpSettingsAction;
	private Action dhcpOnAction;
	private Action dhcpOffAction;
	private Action dnsAutoOnAction;
	private Action dnsAutoOffAction;
	private Action changeProxySettingsAction;
	private Action proxyEnabledAction;

	private JTextField aliasTextField;
	private JCheckBox enabledCheckBox;
	private DefaultComboBoxModel<String> adapterComboModel;
	private JComboBox<String> adapterCombo;
	private JCheckBox changeIpSettingsCheckbox;
	private ButtonGroup dhcpButtonGroup;
	private JRadioButton dhcpOnRadioButton;
	private JRadioButton dhcpOffRadioButton;
	private JTextField hostAddressTextField;
	private JTextField netMaskTextField;
	private JTextField defaultGatewayTextField;
	private ButtonGroup dnsAutoButtonGroup;
	private JRadioButton dnsAutoOnRadioButton;
	private JRadioButton dnsAutoOffRadioButton;
	private JTextField primaryDnsTextField;
	private JTextField additionalDnsTextField;
	private JCheckBox changeProxySettingsCheckBox;
	private JCheckBox proxyEnabledCheckBox;
	private JTextField proxyHostTextField;
	private JTextField proxyPortTextField;
	private JTextField proxyExceptionsTextField;

	public ProfilePanel(UiManager manager, int index) {
		super();
		log.debug("[initProfilePanel]");
		this.manager = manager;
		this.index = index;
		this.init();
	}

	public void updateIndex(int newIndex) {
		this.index = newIndex;
	}

	public void loadValues(Profile profile) {
		disableTriggers = true;
		manager.invoke(new Invocable() {
			@Override
			public void run(UiManager manager) {
				if(profile!=null) {
					try {
						aliasTextField.setText(profile.getAlias());
						manager.getMainFrame().updateTabTitle(index, profile.getAlias());
						enabledCheckBox.setSelected(profile.isEnabled());
						manager.getMainFrame().updateComponentStatus();
						adapterComboModel.setSelectedItem(profile.getAdapterName());
						boolean changeIpSettings = profile.isChangeIPSettings();
						changeIpSettingsCheckbox.setSelected(changeIpSettings);
						setIpSettingsEnabled(changeIpSettings);
						setDnsSettingsEnabled(changeIpSettings);
						if(profile.isDhcpEnabled()) {
							dhcpOnRadioButton.setSelected(true);
						} else {
							dhcpOffRadioButton.setSelected(true);
						}
						
						updateDhcp();
						hostAddressTextField.setText(profile.getHostAddress());
						netMaskTextField.setText(profile.getNetMask());
						defaultGatewayTextField.setText(profile.getDefaultGateway());
						if(profile.isManualDns()) {
							dnsAutoOffRadioButton.setSelected(true);
						} else {
							dnsAutoOnRadioButton.setSelected(true);
						}
						updateDns();
						primaryDnsTextField.setText(profile.getPrimaryDns());
						additionalDnsTextField.setText(profile.getAdditionalDns() == null ? ""
								: profile.getAdditionalDns().stream().collect(Collectors.joining(",")));
						changeProxySettingsCheckBox.setSelected(profile.isChangeProxySettings());
						proxyEnabledCheckBox.setSelected(profile.isProxyEnabled());
						updateProxy();
						proxyHostTextField.setText(profile.getProxyHost());
						proxyPortTextField.setText(profile.getProxyPort() == null ? "" : profile.getProxyPort().toString());
						proxyExceptionsTextField.setText(profile.getProxyExceptions());
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				disableTriggers = false;
			}
		});
	}

	public Profile saveValues() {
		Profile result = new Profile();
		result.setAlias(aliasTextField.getText());
		result.setEnabled(enabledCheckBox.isSelected());
		result.setAdapterName((String)adapterComboModel.getSelectedItem());
		result.setChangeIPSettings(changeIpSettingsCheckbox.isSelected());
		result.setDhcpEnabled(dhcpOnRadioButton.isSelected());
		result.setHostAddress(hostAddressTextField.getText());
		result.setNetMask(netMaskTextField.getText());
		result.setDefaultGateway(defaultGatewayTextField.getText());
		result.setManualDns(dnsAutoOffRadioButton.isSelected());
		result.setPrimaryDns(primaryDnsTextField.getText());
		result.setAdditionalDns(Arrays.asList(additionalDnsTextField.getText().split(",")));
		result.setChangeProxySettings(changeProxySettingsCheckBox.isSelected());
		result.setProxyEnabled(proxyEnabledCheckBox.isSelected());
		result.setProxyHost(proxyHostTextField.getText());
		try {
			result.setProxyPort(Integer.parseInt(proxyPortTextField.getText()));
		} catch(Exception e) {}
		result.setProxyExceptions(proxyExceptionsTextField.getText());
		return result;
	}

	private void init() {
		log.debug("[init]");
		initActions();
		
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.getVerticalScrollBar().setUnitIncrement(12);
		
		JPanel scrollContentsPane = new JPanel();
		scrollContentsPane.setLayout(new BorderLayout(5, 5));
		scrollContentsPane.add(new JPanel(), BorderLayout.NORTH);
		scrollContentsPane.add(new JPanel(), BorderLayout.EAST);
		scrollContentsPane.add(new JPanel(), BorderLayout.WEST);
		scrollContentsPane.add(initCenterPanel(), BorderLayout.CENTER);
		this.setViewportView(scrollContentsPane);
		this.revalidate();
		
		this.loadInitialValues();
	}

	private void initActions() {
		log.debug("[initActions]");
		enabledAction = new AbstractAction(Main.getLiteral("profile.enabled")) {
			private static final long serialVersionUID = -2061386360386040345L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("enabledAction.actionPerformed");
			}
		};
		changeIpSettingsAction = new AbstractAction(Main.getLiteral("profile.changeIpSettings")) {
			private static final long serialVersionUID = -9111795491393115258L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("changeIpSettingsAction.actionPerformed");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						boolean selected = changeIpSettingsCheckbox.isSelected();
						setIpSettingsEnabled(selected);
						setDnsSettingsEnabled(selected);
					}
				});
			}
		};
		dhcpOnAction = new AbstractAction(Main.getLiteral("profile.dhcp.on")) {
			private static final long serialVersionUID = -8741871947291115999L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("dhcpOnAction.actionPerformed");
				if(!disableTriggers) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							updateDhcp();
						}
					});
				}
			}
		};
		dhcpOffAction = new AbstractAction(Main.getLiteral("profile.dhcp.off")) {
			private static final long serialVersionUID = 3262999798477088486L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("dhcpOffAction.actionPerformed");
				if(!disableTriggers) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							updateDhcp();
						}
					});
				}
			}
		};
		dnsAutoOnAction = new AbstractAction(Main.getLiteral("profile.autoDns.on")) {
			private static final long serialVersionUID = 7652767147320445252L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("dnsAutoOnAction.actionPerformed");
				if(!disableTriggers) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							updateDns();
						}
					});
				}
			}
		};
		dnsAutoOffAction = new AbstractAction(Main.getLiteral("profile.autoDns.off")) {
			private static final long serialVersionUID = -96850646587102603L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("dnsAutoOffAction.actionPerformed");
				if(!disableTriggers) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							updateDns();
						}
					});
				}
			}
		};
		changeProxySettingsAction = new AbstractAction(Main.getLiteral("profile.changeProxySettings")) {
			private static final long serialVersionUID = 9111597156825488628L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("changeProxySettingsAction.actionPerformed");
				if(!disableTriggers) manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						setProxySettingsEnabled(changeProxySettingsCheckBox.isSelected());
					}
				});
			}
		};
		proxyEnabledAction = new AbstractAction(Main.getLiteral("profile.proxyEnabled")) {
			private static final long serialVersionUID = -1424503480177871394L;
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("proxyEnabledAction.actionPerformed");
				if(!disableTriggers) {
					manager.invoke(new Invocable() {
						@Override
						public void run(UiManager manager) {
							updateProxy();
						}
					});
				}
			}
		};
	}

	private JComponent initCenterPanel() {
		log.debug("[initCenterPanel]");
		JPanel result = new JPanel();
		BoxLayout layout = new BoxLayout(result, BoxLayout.PAGE_AXIS);
		result.setLayout(layout);
		result.add(initHeader());
		result.add(initIpPanel());
		result.add(initProxyPanel());
		result.add(initToolbar());
		return result;
	}

	private JComponent initHeader() {
		log.debug("[initHeader]");
		JPanel result = new JPanel();
		result.setAlignmentX(0);
		result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
		JLabel label = new JLabel(Main.getLiteral("profile.alias"));
		label.setAlignmentX(0);
		result.add(label);
		aliasTextField = new JTextField();
		aliasTextField.setAlignmentX(0);
		result.add(aliasTextField);
		aliasTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						manager.getMainFrame().updateTabTitle(index, aliasTextField.getText());
					}
				});
			}
		});
		enabledCheckBox = new JCheckBox(enabledAction);
		enabledCheckBox.setAlignmentX(0);
		enabledCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("[enabledCheckBox][actionPerformed]");
				manager.invoke(new Invocable() {
					@Override
					public void run(UiManager manager) {
						manager.getMainFrame().updateComponentStatus();
					}
				});
			}
		});
		result.add(enabledCheckBox);
		label = new JLabel(Main.getLiteral("profile.adapterName"));
		label.setAlignmentX(0);
		result.add(label);
		adapterComboModel = new DefaultComboBoxModel<String>();
		adapterCombo = new JComboBox<>(adapterComboModel);
		adapterCombo.setAlignmentX(0);
		result.add(adapterCombo);
		return result;
	}

	private JComponent initIpPanel() {
		log.debug("[initIpPanel]");
		JPanel result = new JPanel();
		result.setAlignmentX(0);
		result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
		result.add(new JPanel());
		changeIpSettingsCheckbox = new JCheckBox(changeIpSettingsAction);
		changeIpSettingsCheckbox.setAlignmentX(0);
		result.add(changeIpSettingsCheckbox);
		JPanel tcpSettingsPanel = new JPanel();
		tcpSettingsPanel.setAlignmentX(0);
		tcpSettingsPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
		tcpSettingsPanel.setLayout(new BoxLayout(tcpSettingsPanel, BoxLayout.Y_AXIS));
		dhcpButtonGroup = new ButtonGroup();
		dhcpOnRadioButton = new JRadioButton(dhcpOnAction);
		dhcpOnRadioButton.setAlignmentX(0);
		dhcpButtonGroup.add(dhcpOnRadioButton);
		tcpSettingsPanel.add(dhcpOnRadioButton);
		dhcpOffRadioButton = new JRadioButton(dhcpOffAction);
		dhcpOffRadioButton.setAlignmentX(0);
		dhcpButtonGroup.add(dhcpOffRadioButton);
		tcpSettingsPanel.add(dhcpOffRadioButton);
		Border compoundBorder = new CompoundBorder(
				new CompoundBorder(
					new EmptyBorder(10, 10, 10, 10), 
					new EtchedBorder(EtchedBorder.LOWERED)
				), 
				new EmptyBorder(10, 10, 10, 10)
			);
		
		JPanel ipSettingsPanel = new JPanel();
		ipSettingsPanel.setAlignmentX(0);
		ipSettingsPanel.setBorder(compoundBorder);
		ipSettingsPanel.setLayout(new BoxLayout(ipSettingsPanel, BoxLayout.Y_AXIS));
		JLabel label = new JLabel(Main.getLiteral("profile.hostAddress"));
		label.setAlignmentX(0);
		ipSettingsPanel.add(label);
		hostAddressTextField = new JTextField();
		hostAddressTextField.setAlignmentX(0);
		ipSettingsPanel.add(hostAddressTextField);
		label = new JLabel(Main.getLiteral("profile.netMask"));
		label.setAlignmentX(0);
		ipSettingsPanel.add(label);
		netMaskTextField = new JTextField();
		netMaskTextField.setAlignmentX(0);
		ipSettingsPanel.add(netMaskTextField);
		label = new JLabel(Main.getLiteral("profile.defaultGateway"));
		label.setAlignmentX(0);
		ipSettingsPanel.add(label);
		defaultGatewayTextField = new JTextField();
		defaultGatewayTextField.setAlignmentX(0);
		ipSettingsPanel.add(defaultGatewayTextField);
		tcpSettingsPanel.add(ipSettingsPanel);
		dnsAutoButtonGroup = new ButtonGroup();
		dnsAutoOnRadioButton = new JRadioButton(dnsAutoOnAction);
		dnsAutoOnRadioButton.setAlignmentX(0);
		dnsAutoButtonGroup.add(dnsAutoOnRadioButton);
		tcpSettingsPanel.add(dnsAutoOnRadioButton);
		dnsAutoOffRadioButton = new JRadioButton(dnsAutoOffAction);
		dnsAutoOffRadioButton.setAlignmentX(0);
		dnsAutoButtonGroup.add(dnsAutoOffRadioButton);
		tcpSettingsPanel.add(dnsAutoOffRadioButton);
		JPanel dnsSettingsPanel = new JPanel();
		dnsSettingsPanel.setAlignmentX(0);
		dnsSettingsPanel.setBorder(compoundBorder);
		dnsSettingsPanel.setLayout(new BoxLayout(dnsSettingsPanel, BoxLayout.Y_AXIS));
		label = new JLabel(Main.getLiteral("profile.primaryDns"));
		label.setAlignmentX(0);
		dnsSettingsPanel.add(label);
		primaryDnsTextField = new JTextField();
		primaryDnsTextField.setAlignmentX(0);
		dnsSettingsPanel.add(primaryDnsTextField);
		label = new JLabel(Main.getLiteral("profile.additionalDns"));
		label.setAlignmentX(0);
		dnsSettingsPanel.add(label);
		additionalDnsTextField = new JTextField();
		additionalDnsTextField.setAlignmentX(0);
		dnsSettingsPanel.add(additionalDnsTextField);
		tcpSettingsPanel.add(dnsSettingsPanel);
		result.add(tcpSettingsPanel);
		return result;
	}

	private JComponent initProxyPanel() {
		log.debug("[initProxyPanel]");
		JPanel result = new JPanel();
		result.setAlignmentX(0);
		result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
		result.add(new JPanel());
		changeProxySettingsCheckBox = new JCheckBox(changeProxySettingsAction);
		changeProxySettingsCheckBox.setAlignmentX(0);
		result.add(changeProxySettingsCheckBox);
		JPanel proxySettingsPanel = new JPanel();
		proxySettingsPanel.setAlignmentX(0);
		proxySettingsPanel.setBorder(
			new CompoundBorder(
				new EtchedBorder(EtchedBorder.LOWERED),
				new EmptyBorder(10, 10, 10, 10)
			)
		);
		proxySettingsPanel.setLayout(new BoxLayout(proxySettingsPanel, BoxLayout.Y_AXIS));
		
		proxyEnabledCheckBox = new JCheckBox(proxyEnabledAction);
		proxyEnabledCheckBox.setAlignmentX(0);
		proxySettingsPanel.add(proxyEnabledCheckBox);
		
		JLabel label = new JLabel(Main.getLiteral("profile.proxyHost"));
		label.setAlignmentX(0);
		proxySettingsPanel.add(label);
		proxyHostTextField = new JTextField();
		proxyHostTextField.setAlignmentX(0);
		proxySettingsPanel.add(proxyHostTextField);
		
		label = new JLabel(Main.getLiteral("profile.proxyPort"));
		label.setAlignmentX(0);
		proxySettingsPanel.add(label);
		proxyPortTextField = new JTextField();
		proxyPortTextField.setAlignmentX(0);
		proxySettingsPanel.add(proxyPortTextField);
		
		label = new JLabel(Main.getLiteral("profile.proxyExceptions"));
		label.setAlignmentX(0);
		proxySettingsPanel.add(label);
		proxyExceptionsTextField = new JTextField();
		proxyExceptionsTextField.setAlignmentX(0);
		proxySettingsPanel.add(proxyExceptionsTextField);
		
		result.add(proxySettingsPanel);
		return result;
	}

	private JComponent initToolbar() {
		log.debug("[initToolbar]");
		JPanel result = new JPanel();
		result.setAlignmentX(0);
		
		return result;
	}

	public void completeInit() {
		aliasTextField.setMaximumSize(aliasTextField.getSize());
		aliasTextField.setMinimumSize(aliasTextField.getSize());
		adapterCombo.setMaximumSize(adapterCombo.getSize());
		adapterCombo.setMinimumSize(adapterCombo.getSize());
		hostAddressTextField.setMaximumSize(hostAddressTextField.getSize());
		hostAddressTextField.setMinimumSize(hostAddressTextField.getSize());
		netMaskTextField.setMaximumSize(netMaskTextField.getSize());
		netMaskTextField.setMinimumSize(netMaskTextField.getSize());
		defaultGatewayTextField.setMaximumSize(defaultGatewayTextField.getSize());
		defaultGatewayTextField.setMinimumSize(defaultGatewayTextField.getSize());
		primaryDnsTextField.setMaximumSize(primaryDnsTextField.getSize());
		primaryDnsTextField.setMinimumSize(primaryDnsTextField.getSize());
		additionalDnsTextField.setMaximumSize(additionalDnsTextField.getSize());
		additionalDnsTextField.setMinimumSize(additionalDnsTextField.getSize());
		proxyHostTextField.setMaximumSize(proxyHostTextField.getSize());
		proxyHostTextField.setMinimumSize(proxyHostTextField.getSize());
		proxyPortTextField.setMaximumSize(proxyPortTextField.getSize());
		proxyPortTextField.setMinimumSize(proxyPortTextField.getSize());
		proxyExceptionsTextField.setMaximumSize(proxyExceptionsTextField.getSize());
		proxyExceptionsTextField.setMinimumSize(proxyExceptionsTextField.getSize());
	}

	private void loadInitialValues() {
		this.aliasTextField.setText(Main.getLiteral("newProfileLabel"));
		this.adapterComboModel.removeAllElements();
		for(String adapter : ProfileManager.getAdapterNames()) {
			adapterComboModel.addElement(adapter);
		}
		setIpSettingsEnabled(false);
		setDnsSettingsEnabled(false);
		setProxySettingsEnabled(false);
	}

	private void updateDhcp() {
		boolean dhcpOn = dhcpOnRadioButton.isSelected();
		setDhcp(dhcpOn);
		dnsAutoOnAction.setEnabled(dhcpOn);
		if(!dhcpOn) {
			dnsAutoOffRadioButton.setSelected(true);
			setAutoDns(false);
		}
	}

	private void updateDns() {
		setAutoDns(dnsAutoOnRadioButton.isSelected());
	}

	private void updateProxy() {
		proxyEnabledCheckBox.setEnabled(changeProxySettingsCheckBox.isSelected());
		setProxyEnabled(proxyEnabledCheckBox.isSelected());
	}

	private void setIpSettingsEnabled(boolean state) {
		dhcpOnAction.setEnabled(state);
		dhcpOffAction.setEnabled(state);
		if(!state) {
			dhcpOnRadioButton.setSelected(true);
			setDhcp(true);
		}
	}

	private void setDhcp(boolean mode) {
		hostAddressTextField.setEnabled(!mode);
		netMaskTextField.setEnabled(!mode);
		defaultGatewayTextField.setEnabled(!mode);
		if(mode) {
			hostAddressTextField.setText("");
			netMaskTextField.setText("");
			defaultGatewayTextField.setText("");
		}
	}

	private void setDnsSettingsEnabled(boolean state) {
		dnsAutoOnAction.setEnabled(state);
		dnsAutoOffAction.setEnabled(state);
		if(!state) {
			dnsAutoOnRadioButton.setSelected(true);
			setAutoDns(true);
		}
	}

	private void setAutoDns(boolean mode) {
		primaryDnsTextField.setEnabled(!mode);
		additionalDnsTextField.setEnabled(!mode);
		if(mode) {
			primaryDnsTextField.setText("");
			additionalDnsTextField.setText("");
		}
	}

	private void setProxySettingsEnabled(boolean state) {
		proxyEnabledAction.setEnabled(state);
		if(!state) {
			proxyEnabledCheckBox.setSelected(false);
			setProxyEnabled(false);
		}
	}

	private void setProxyEnabled(boolean mode) {
		proxyHostTextField.setEnabled(mode);
		proxyPortTextField.setEnabled(mode);
		proxyExceptionsTextField.setEnabled(mode);
		if(!mode) {
			proxyHostTextField.setText("");
			proxyPortTextField.setText("");
			proxyExceptionsTextField.setText("");
		}
	}
}
