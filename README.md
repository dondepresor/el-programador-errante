# El Programador Errante

Aplicación de gestión de perfiles de configuración de red para Windows 10

![El Programador Errante](wd.png)

Permite configurar los siguientes aspectos de la conexión de red:

- Dirección IP manual o por DHCP
- Puerta de enlace
- Servidores DNS primario y secundario
- Proxy de Windows

Al aplicar el perfil, se ejecutan los comandos de Windows que aplican la configuración

![Aplicando la configuración](wd-2.png)

Los perfiles se pueden lanzar directamente desde el icono situado en la bandeja del sistema

![El menu de la bandeja del sistema](wd-3.png)

## Instalación

Compila la aplicación mediante el comando:

```
mvn clean install
```

O bien desde *Eclipse*, seleccionando `Maven` > `Clean` y después `Maven` > `Install`.

Después, copia la carpeta `target\wd` a su ubicación de destino.

Finalmente, ejecuta el script de instalación `install.bat`.

Esto creará:

- Un acceso directo en el escritorio
- Un acceso directo en la carpeta Inicio del menú de inicio
- Un script de desinstalación

## Ejecución

Al ejecutar la aplicación, se mostrará un cuadro de diálogo solicitando permisos de administración. Esto es necesario dado que la aplicación cambia la configuración de red del sistema.
